

#include <TimerOne.h>           // Avaiable from http://www.arduino.cc/playground/Code/Timer1


byte maxDim = 45;     // 128 = off / 45 barely perceptable
byte minDim = 0;       // 0 = on

float dimDiff = 0.05;

int waveLength = 7000; // one full sin curve in millis

int AC_pin1 = 8;                 // Output to Opto Triac
int AC_pin2 = 9;
int AC_pin3 = 11;
int AC_pin4 = 10;

float dim1 = 128;                 // Dimming level (0-128)  0 = on, 128 = 0ff
float dim2 = 128;
float dim3 = 128;
float dim4 = 128;

volatile int i = 0;               // Variable to use as a counter            
volatile boolean zero_cross = 0;  // Boolean to store a "switch" to tell us if we have crossed zero
//int freqStep = 45;                // This is the delay-per-brightness step in microseconds.
int freqStep = 65; 

// It is calculated based on the frequency of your voltage supply (50Hz or 60Hz)
// and the number of brightness steps you want. 
// 
// The only tricky part is that the chopper circuit chops the AC wave twice per
// cycle, once on the positive half and once at the negative half. This meeans
// the chopping happens at 120Hz for a 60Hz supply or 100Hz for a 50Hz supply. 

// To calculate freqStep you divide the length of one full half-wave of the power
// cycle (in microseconds) by the number of brightness steps. 
//
// (1000000 uS / 120 Hz) / 128 brightness steps = 65 uS / brightness step
//
// 1000000 us / 120 Hz = 8333 uS, length of one half-wave.

void setup() {                                      // Begin setup
  Serial.begin(9600);
  pinMode(AC_pin1, OUTPUT);                          // Set the Triac pin as output
  pinMode(AC_pin2, OUTPUT);
  pinMode(AC_pin3, OUTPUT);
  pinMode(AC_pin4, OUTPUT);
  
  attachInterrupt(0, zero_cross_detect, RISING);   // Attach an Interupt to Pin 2 (interupt 0) for Zero Cross Detection
  Timer1.initialize(freqStep);                      // Initialize TimerOne library for the freq we need 
  Timer1.attachInterrupt(dim_check, freqStep); 
  
  // Use the TimerOne Library to attach an interrupt
  // to the function we use to check to see if it is 
  // the right time to fire the triac.  This function 
  // will now run every freqStep in microseconds.   
}

void zero_cross_detect() {    
  zero_cross = true;               // set the boolean to true to tell our dimming function that a zero cross has occured
  i=0;
  digitalWrite(AC_pin1, LOW);       // turn off TRIAC (and AC)
  digitalWrite(AC_pin2, LOW); 
  digitalWrite(AC_pin3, LOW); 
  digitalWrite(AC_pin4, LOW); 
}  

// Turn on the TRIAC at the appropriate time
void dim_check() {                   
  if(zero_cross == true) {  
    
    if(i>=dim1) {                     
      digitalWrite(AC_pin1, HIGH); // turn on light
    } 
    if(i>=dim2) {                     
      digitalWrite(AC_pin2, HIGH); // turn on light
    }  
    if(i>=dim3) {                     
      digitalWrite(AC_pin3, HIGH); // turn on light
    } 
    if(i>=dim4) {                     
      digitalWrite(AC_pin4, HIGH); // turn on light
    }    
    
    i++; // increment time step counter                                                        
  }                                  
}    

void loop() {    
  dim1 = ((maxDim-minDim)/2)*(sin((1)*millis()*TWO_PI/waveLength)+1) +minDim;
  dim2 = ((maxDim-minDim)/2)*(sin((1+dimDiff)*millis()*TWO_PI/waveLength)+1) +minDim;
  dim3 = ((maxDim-minDim)/2)*(sin((1+dimDiff*2)*millis()*TWO_PI/waveLength)+1) +minDim;
  dim4 = ((maxDim-minDim)/2)*(sin((1+dimDiff*3)*millis()*TWO_PI/waveLength)+1) +minDim;
  
  Serial.print(dim1,0); Serial.print(","); Serial.print(dim2,0); 
  Serial.print(","); Serial.print(dim3,0); Serial.print(","); Serial.println(dim4,0);
}





